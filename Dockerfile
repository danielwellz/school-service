# Use the official Node.js image with version 18.17.0
FROM node:18.17.0-alpine

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code to the container
COPY . .

# Build the NestJS app
RUN npm run build

# Expose the port your NestJS app is running on (default is 3000)
EXPOSE 3000

# Start your NestJS app
CMD ["node", "dist/main"]