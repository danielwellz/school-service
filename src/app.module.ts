import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SchoolModule } from './schools/school.module'; // Import the SchoolsModule
import { School } from './schools/entities/school.entity'; // Import the School entity

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'school_database',
      entities: [School], // Add your entity classes here
      synchronize: true, // Auto-generate and sync the database schema (in development)
    }),
    SchoolModule, // Add the SchoolsModule to the imports array
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
