import { IsNotEmpty, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateSchoolDto {
  @ApiProperty({
    description: 'Name of the school',
    example: 'New School Name',
    minLength: 2,
    maxLength: 50,
  })
  @MaxLength(50, { message: 'Name should not exceed 50 characters' })
  @IsNotEmpty({ message: 'Name should not be empty' })
  name: string;

  @ApiProperty({
    description: 'Address of the school',
    example: '123 New Street',
    minLength: 5,
    maxLength: 100,
  })
  @MaxLength(100, { message: 'Address should not exceed 100 characters' })
  @IsNotEmpty({ message: 'Address should not be empty' })
  address: string;
}
