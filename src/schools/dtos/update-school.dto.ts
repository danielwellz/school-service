import { IsNotEmpty, IsOptional, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { PartialType } from '@nestjs/mapped-types';

export class UpdateSchoolDto {
  @ApiProperty({
    description: 'Name of the school',
    example: 'Example School',
    minLength: 2,
    maxLength: 50,
  })
  @IsNotEmpty({ message: 'Name should not be empty' })
  @MaxLength(50, { message: 'Name should not exceed 50 characters' })
  @IsOptional()
  name?: string;

  @ApiProperty({
    description: 'Address of the school',
    example: '123 Example Street',
    minLength: 5,
    maxLength: 100,
  })
  @IsNotEmpty({ message: 'Address should not be empty' })
  @MaxLength(100, { message: 'Address should not exceed 100 characters' })
  @IsOptional()
  address?: string;
}

export class PartialUpdateSchoolDto extends PartialType(UpdateSchoolDto) {}
