import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Delete,
  Param,
  HttpStatus,
  NotFoundException,
  BadRequestException,
  Logger,
} from '@nestjs/common';
import { SchoolService } from './school.service';
import { School } from './entities/school.entity';
import { CreateSchoolDto } from './dtos/create-school.dto';
import { UpdateSchoolDto } from './dtos/update-school.dto';
import {
  ApiTags,
  ApiResponse,
  ApiBadRequestResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger';

@ApiTags('schools')
@Controller('schools')
export class SchoolController {
  private readonly logger = new Logger(SchoolController.name);

  constructor(private readonly schoolService: SchoolService) {}

  @Get()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get all schools',
    type: [School],
  })
  async getSchools(): Promise<School[]> {
    this.logger.verbose('Fetching all schools');
    return this.schoolService.getSchools();
  }

  @Post('create')
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Create a new school',
    type: School,
  })
  async createSchool(
    @Body() createSchoolDto: CreateSchoolDto,
  ): Promise<School> {
    try {
      this.logger.debug('Creating new school');
      return await this.schoolService.createSchool(createSchoolDto);
    } catch (error) {
      this.logger.error('Failed to create school');
      throw new BadRequestException('Failed to create school');
    }
  }

  @Get(':id')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get school by ID',
    type: School,
  })
  @ApiNotFoundResponse({ description: 'School not found' })
  async getSchoolById(@Param('id') id: number): Promise<School> {
    try {
      this.logger.debug(`Fetching school with ID ${id}`);
      return await this.schoolService.getSchoolById(id);
    } catch (error) {
      this.logger.error(`School with ID ${id} not found`);
      throw new NotFoundException(`School with ID ${id} not found`);
    }
  }

  @Patch('update/:id')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Update school by ID',
    type: School,
  })
  @ApiBadRequestResponse({ description: 'Invalid data provided' })
  @ApiNotFoundResponse({ description: 'School not found' })
  async updateSchool(
    @Param('id') id: number,
    @Body() updateSchoolDto: UpdateSchoolDto,
  ): Promise<School> {
    try {
      this.logger.debug(`Updating school with ID ${id}`);
      const updatedSchool = await this.schoolService.updateSchool(
        id,
        updateSchoolDto,
      );
      if (!updatedSchool) {
        throw new NotFoundException(`School with ID ${id} not found`);
      }
      return updatedSchool;
    } catch (error) {
      this.logger.error('Failed to update school');
      throw new BadRequestException('Failed to update school');
    }
  }

  @Delete('delete/:id')
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Delete school by ID',
  })
  @ApiNotFoundResponse({ description: 'School not found' })
  async deleteSchool(@Param('id') id: number): Promise<void> {
    try {
      this.logger.debug(`Deleting school with ID ${id}`);
      await this.schoolService.deleteSchool(id);
    } catch (error) {
      this.logger.error('Failed to delete school');
      throw new BadRequestException('Failed to delete school');
    }
  }

  @Post('create-sample')
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Create 10 sample schools',
    type: [School],
  })
  async createSampleSchools(): Promise<School[]> {
    try {
      this.logger.debug('Creating 10 sample schools');
      return this.schoolService.createRandomSchools();
    } catch (error) {
      this.logger.error('Failed to create sample schools');
      throw new BadRequestException('Failed to create sample schools');
    }
  }
}
