import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { School } from './entities/school.entity';

@Injectable()
export class SchoolRepository {
  constructor(
    @InjectRepository(School)
    private readonly repository: Repository<School>,
  ) {}
}
