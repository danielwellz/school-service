import {
  Injectable,
  Logger,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { Repository } from 'typeorm';
import { School } from './entities/school.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateSchoolDto } from './dtos/create-school.dto';
import { PartialUpdateSchoolDto } from './dtos/update-school.dto';

@Injectable()
export class SchoolService {
  private readonly logger = new Logger(SchoolService.name);

  constructor(
    @InjectRepository(School) private schoolRepository: Repository<School>,
  ) {}

  async createSchool(createSchoolDto: CreateSchoolDto): Promise<School> {
    try {
      const newSchool = this.schoolRepository.create(createSchoolDto);
      const savedSchool = await this.schoolRepository.save(newSchool);
      this.logger.log(`Created school with ID ${savedSchool.id}`);
      return savedSchool;
    } catch (error) {
      this.logger.error(`Failed to create school. Error: ${error.message}`);
      throw error;
    }
  }

  async updateSchool(
    id: number,
    updateSchoolDto: PartialUpdateSchoolDto,
  ): Promise<School> {
    try {
      await this.schoolRepository.update(id, updateSchoolDto);
      const updatedSchool = await this.getSchoolById(id);
      this.logger.log(`Updated school with ID ${id}`);
      return updatedSchool;
    } catch (error) {
      this.logger.error(`Failed to update school. Error: ${error.message}`);
      throw error;
    }
  }

  async deleteSchool(id: number): Promise<void> {
    try {
      await this.schoolRepository.delete(id);
      this.logger.log(`Deleted school with ID ${id}`);
    } catch (error) {
      this.logger.error(`Failed to delete school. Error: ${error.message}`);
      throw error;
    }
  }

  async getSchools(): Promise<School[]> {
    return this.schoolRepository.find();
  }

  async getSchoolById(id: number): Promise<School> {
    try {
      const school = await this.schoolRepository.findOne({ where: { id } });
      if (!school) {
        throw new NotFoundException(`School with ID ${id} not found`);
      }
      return school;
    } catch (error) {
      this.logger.error(`Failed to get school by ID. Error: ${error.message}`);
      throw error;
    }
  }

  async createRandomSchools(): Promise<School[]> {
    try {
      const randomSchools: CreateSchoolDto[] = [];
      for (let i = 1; i <= 10; i++) {
        randomSchools.push({
          name: `Random School ${i}`,
          address: `${Math.floor(Math.random() * 1000)} Random Street`,
        });
      }

      const createdSchools: School[] = [];
      for (const schoolDto of randomSchools) {
        const createdSchool = await this.createSchool(schoolDto);
        createdSchools.push(createdSchool);
      }
      this.logger.log('Created 10 random schools');
      return createdSchools;
    } catch (error) {
      this.logger.error(
        `Failed to create random schools. Error: ${error.message}`,
      );
      throw error;
    }
  }
}
